# Atic Atac Mod #

[Atic Atac](https://en.wikipedia.org/wiki/Atic%5FAtac) is a [ZX Spectrum](https://en.wikipedia.org/wiki/ZX%5FSpectrum) game published in 1983 by [Ultimate Play the Game](https://en.wikipedia.org/wiki/Ultimate%5FPlay%5Fthe%5FGame).

The game uses keys _Q_/_W_/_E_/_R_/_T_ for the _Left_/_Right_/_Down_/_Up_/_Fire_ actions, which some people find awkward to use.

This mod adds the ability to redefine your own keys. 

It also add a convenience key to quit the current game. The quit key is not assigned or active until you redefine keys for the first time.

## Tape Files ##
[AticAtac-RedefineMod.tap](https://bitbucket.org/Threetwosevensixseven/aticatacmod/raw/master/bin/AticAtac-RedefineMod.tap)   
[AticAtac-RedefineMod.tzx](https://bitbucket.org/Threetwosevensixseven/aticatacmod/raw/master/bin/AticAtac-RedefineMod.tzx)   
[AticAtac-RedefineMod.z80](https://bitbucket.org/Threetwosevensixseven/aticatacmod/raw/master/bin/AticAtac-RedefineMod.z80) - DivMMC-friendly snapshot.   
[AticAtac-RedefineMod-MMC24.tap](https://bitbucket.org/Threetwosevensixseven/aticatacmod/raw/master/bin/AticAtac-RedefineMod-MMC24.tap) - Archie Robins' divMMC-friendly tap.   

## Screenshots ##
### Modified Menu ###
![Modified menu](https://bytebucket.org/Threetwosevensixseven/aticatacmod/raw/9809e4a19c4c377d6e4639b5f0a9c4c109a3ff25/images/aticatac-mod-1.png "Modified menu")

### Redefine Screen ###
![Redefine screen](https://bytebucket.org/Threetwosevensixseven/aticatacmod/raw/9809e4a19c4c377d6e4639b5f0a9c4c109a3ff25/images/aticatac-mod-2.png "Redefine screen")

### Confirming Keys ###
![Confirming keys](https://bytebucket.org/Threetwosevensixseven/aticatacmod/raw/9809e4a19c4c377d6e4639b5f0a9c4c109a3ff25/images/aticatac-mod-3.png "Confirming keys")

### Confirming Quit ###
![Confirming quit](https://bytebucket.org/Threetwosevensixseven/aticatacmod/raw/9809e4a19c4c377d6e4639b5f0a9c4c109a3ff25/images/aticatac-mod-4.png "Confirming quit")

## Assembling ##

Install [Zeus](http://www.desdes.com/products/oldfiles/zeus.htm) Z80 cross-assembler for Windows.

Open [/src/main.asm](https://bitbucket.org/Threetwosevensixseven/aticatacmod/src/master/src/main.asm), and click the _Assemble Then Emulate_ button.

## Copyright/Disclaimer ##

The copyright remains with [Rare](https://www.rare.co.uk/)/[Microsoft](https://www.microsoft.com/).

This mod was made as a non-commercial fan version, with thanks and respect to all the developers, publishers and rights holders.

