; main.asm

; This code file sets up the environment in Zeus,
; loads the original game and the code patches,
; then creates TAP and TZX files of the fully-patched game.

zeusemulate             "48K"
ZEUS_PC                 equ EntryPoint

import_bin              "game.bin", Game1Start          ; Memory dump of original game.
include                 "original.asm"                  ; Original code for debugging step-through.
include                 "custom.asm"                    ; Custom functionality added to the game.
include                 "hook.asm"                      ; Custom hook routines to be patched into the game.
include                 "data.asm"                      ; Database tables used by custom and hooked routines.
include                 "patch.asm"                     ; The patching is done in here.
include                 "constants.asm"                 ; Constants to make the code more readable.
include                 "macros.asm"                    ; Helper macros to make the code more readable.
PatchEnd                equ $-1                         ; Upper extent of custom code.

                        if zeusver < 64
                          zeuserror "Upgrade to Zeus v3.69 or above, available at http://www.desdes.com/products/oldfiles/zeus.htm."
                        endif

import_bin              "AticAtac.scr", SCREEN          ; Add a loading screen for the tape files.

                                                        ; Create a TAP file using Zeus's Mode 3 custom loader.
output_tap              "..\bin\AticAtac-RedefineMod.tap", "AticAtac", "", Block1Start, Block1Len, 3, EntryPoint, Mode3Options
output_tap_block        "..\bin\AticAtac-RedefineMod.tap", Block2Start, Block2Len
output_tap_block        "..\bin\AticAtac-RedefineMod.tap", Block3Start, Block3Len

                                                        ; Create a TZX file using Zeus's Mode 3 custom loader.
output_tzx              "..\bin\AticAtac-RedefineMod.tzx", "AticAtac", "", Block1Start, Block1Len, 3, EntryPoint, Mode3Options
output_tzx_block        "..\bin\AticAtac-RedefineMod.tzx", Block2Start, Block2Len
output_tzx_block        "..\bin\AticAtac-RedefineMod.tzx", Block3Start, Block3Len

                                                        ; Create a Z80 snapshot file that is DivMMC-friendly.
output_z80              "..\bin\AticAtac-RedefineMod.z80", 0, EntryPoint


