; macros.asm

; This file contains helper macros to do some of the heavy lifting
; and make the code more readable.

CalculateFontAddress    macro(ASCII)
                        if (ASCII<32 or ASCII>127)
                          zeuserror "ASCII out of range."
                        endif
                        ::FontAddress = AticAtacFont+((ASCII-32)*8)
mend




SaveScreen              macro(X, Y, BufferLine)
                        SrcAddr := zxpixeladdr(X, Y)
                        DstAddr := (Buffer.Pixels)+(BufferLine*8*9)
                        ld hl, SrcAddr
                        ld de, DstAddr
                        call SavePixels
                        SrcAddr := zxattraddr(X, Y)
                        DstAddr := Buffer.Attrs+(BufferLine*9)
                        ld hl, SrcAddr
                        ld de, DstAddr
                        call CopyAttributes
mend



RestScreen              macro(X, Y, BufferLine)
                        SrcAddr := Buffer.Pixels+(BufferLine*8*9)
                        DstAddr := zxpixeladdr(X, Y)
                        ld hl, SrcAddr
                        ld de, DstAddr
                        call RestorePixels
                        SrcAddr := Buffer.Attrs+(BufferLine*9)
                        DstAddr := zxattraddr(X, Y)
                        ld hl, SrcAddr
                        ld de, DstAddr
                        call CopyAttributes
mend

