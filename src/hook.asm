; hook.asm

; This file contains custom hook routines to be patched into the game
; instead of the original routines.

MoveKeysHook            proc                            ; This is the custom routine
                        ld de, 1                        ; that reads the redefined move keys.

FireKeyCode equ $+1:    ld a, K_FireCode                ; Read redefined FIRE matrix code.
                        in a, ($FE)
FireKeyMask equ $+1:    and K_FireMask                  ; Read redefined FIRE matrix mask.
                        sub e
                        rl d                            ; Rotate FIRE bit value into d (active low).

UpKeyCode equ $+1:      ld a, K_UpCode                  ; Read redefined UP matrix code.
                        in a, ($FE)
UpKeyMask equ $+1:      and K_UpMask                    ; Read redefined UP matrix mask.
                        sub e
                        rl d                            ; Rotate UP bit value into d (active low).

DownKeyCode equ $+1:    ld a, K_DownCode                ; Read redefined DOWN matrix code.
                        in a, ($FE)
DownKeyMask equ $+1:    and K_DownMask                  ; Read redefined DOWN matrix mask.
                        sub e
                        rl d                            ; Rotate DOWN bit value into d (active low).

LeftKeyCode equ $+1:    ld a, K_LeftCode                ; Read redefined LEFT matrix code.
                        in a, ($FE)
LeftKeyMask equ $+1:    and K_LeftMask                  ; Read redefined LEFT matrix mask.
                        sub e
                        rl d                            ; Rotate FIRE bit LEFT into d (active low).

RightKeyCode equ $+1:   ld a, K_RightCode               ; Read redefined RIGHT matrix code.
                        in a, ($FE)
RightKeyMask equ $+1:   and K_RightMask                 ; Read redefined RIGHT matrix mask.
                        sub e
                        rl d                            ; Rotate RIGHT bit value into d (active low).

                        ld a, d
                        cpl
                        and %00011111                   ; Return with A containing 000FUDLR
                        ret                             ; (Kempston format).
pend



DropKeyHook             proc                            ; Custom routine that reads the redefined DROP key (only one key!).
DropKeyCode equ $+1:    ld a, K_DropCode                ; Read redefined DROP matrix code.
                        out ($FD), a
                        in a, ($FE)
                        cpl
DropKeyMask equ $+1:    and K_DropMask                  ; Read redefined DROP matrix mask.
                        jr z, NoDrop                    ; a = 0 (no drop)...
Drop:
                        ld a, 2                         ; or 2 (drop).
NoDrop:
                        ld ($5E20), a                   ; Save drop value
                        ret                             ; and return.
pend


                                                        ; Custom routine that reads the redefined PAUSE key (only one key!).
PauseKeyHook            proc                            ; Always unpaused when we enter (state machine STATE 0).
QuitKeyCode equ $+1:    ld a, K_QuitCode                ; Read redefined QUIT matrix code.
                        out ($FD), a
                        in a, ($FE)
                        cpl
QuitKeyMask equ $+1:    and K_QuitMask                  ; Read redefined QUIT matrix mask.
                        jp z, Pause                     ; If not quitting, test PAUSE key.

                        halt

                        SaveScreen((QX+0)*8,(QY+0)*8,0) ; Save portion of screen
                        SaveScreen((QX+0)*8,(QY+1)*8,1) ; Which will be covered up
                        SaveScreen((QX+0)*8,(QY+2)*8,2) ; by the quit confirm question
                        SaveScreen((QX+0)*8,(QY+3)*8,3) ; to the buffer.
                        SaveScreen((QX+0)*8,(QY+4)*8,4)
                        SaveScreen((QX+0)*8,(QY+5)*8,5)
                        SaveScreen((QX+0)*8,(QY+6)*8,6)

                        ld a, 2                         ; 2 is the stream for the upper screen.
                        call CHAN_OPEN                  ; Open channel (ROM).
                        ld de, ConfirmQuit.Table        ; Address of string to print.
                        ld bc, ConfirmQuit.Len          ; Length of string to print.
                        call PR_STRING                  ; Print string (ROM).

                        halt

WaitForNoKeys:
                        xor a                           ; Repeat until no keys
                        in a, ($FE)                     ; are pressed.
                        cpl
                        and %11111
                        jr nz, WaitForNoKeys
ReadYN:
                        ld bc, K_BNMSsSp                ; Read "N" key.
                        in a, (c)
                        cpl
                        and %01000
                        jp nz, NoQuit

                        ld bc, K_YUIOP                  ; Read "Y" key.
                        in a, (c)
                        cpl
                        and %10000
                        jr nz, Quit

                        jr ReadYN                       ; Repeat until either Y or N is pressed.

Quit:
                        di                              ; Rebalance the stack,
                        ld sp, $5DFE                    ; just in case we were in
                        ei                              ; the middle of something weird.
                        jp MainMenuLoop                 ; Then jump to the main menu.
NoQuit:
                        RestScreen((QX+0)*8,(QY+0)*8,0) ; Save portion of screen
                        RestScreen((QX+0)*8,(QY+1)*8,1) ; Which will be covered up
                        RestScreen((QX+0)*8,(QY+2)*8,2) ; by the quit confirm question
                        RestScreen((QX+0)*8,(QY+3)*8,3) ; to the buffer.
                        RestScreen((QX+0)*8,(QY+4)*8,4)
                        RestScreen((QX+0)*8,(QY+5)*8,5)
                        RestScreen((QX+0)*8,(QY+6)*8,6)
Pause:
                        di                              ; Disable interrupts (part of the actual pause mechanism).
PauseKeyCode1 equ $+1:  ld a, K_PauseCode               ; Read redefined PAUSE matrix code.
                        out ($FD), a
                        in a, ($FE)
                        cpl
PauseKeyMask1 equ $+1:  and K_PauseMask                 ; Read redefined PAUSE matrix mask.
                        ret z                           ; Return if key not pressed.
UnpauseRelease:                                         ; We are now pausing (state machine STATE 1).
PauseKeyCode2 equ $+1:  ld a, K_PauseCode               ; Read redefined PAUSE matrix code again.
                        out ($FD), a
                        in a, ($FE)
                        cpl
PauseKeyMask2 equ $+1:  and K_PauseMask                 ; Read redefined PAUSE matrix mask again.
                        jr nz, UnpauseRelease           ; Repeat until key is not pressed.
UnpausePress:                                           ; We are now paused (state machine STATE 2).
PauseKeyCode3 equ $+1:  ld a, K_PauseCode               ; Read redefined PAUSE matrix code again.
                        out ($FD), a
                        in a, ($FE)
                        cpl
PauseKeyMask3 equ $+1:  and K_PauseMask                 ; Read redefined PAUSE matrix mask again.
                        jr z, UnpausePress              ; Repeat until key is pressed.
UnpauseRelease2:                                        ; We are now unpausing (state machine STATE 3).
PauseKeyCode4 equ $+1:  ld a, K_PauseCode               ; Read redefined PAUSE matrix code again.
                        out ($FD), a
                        in a, ($FE)
                        cpl
PauseKeyMask4 equ $+1:  and K_PauseMask                 ; Read redefined PAUSE matrix mask again.
                        jr nz, UnpauseRelease2          ; Repeat until key is not pressed.
                        ret                             ; We are now unpaused (state machine STATE 0).
pend                                                    ; Return. Interrupts will be re-enabled later(?).



MenuKeysHook            proc
                        ld hl, $BE4C                    ; Run the original code
                        call MenuTextAddr+3             ; that we overwrote with the hook.
                        ei
                        ret
pend



MenuPrintHook           proc
                        call MenuIconsAddr              ; Print menu first.
                        ld a, ($584B)                   ; Read attribute for option 1 (keyboard).
                        and %10000000                   ; Isolate flash bit.
                        rlca                            ; 0 = not flashing, 1 = flashing.
                        ld (MainMenu.Flashing), a       ; Print "/REDEFINE" text with correct flash status.
                        ld a, 2                         ; Upper screen.
                        call CHAN_OPEN                  ; Open channel (ROM).
                        ld de, MainMenu.Table           ; Address of string to print.
                        ld bc, MainMenu.Len             ; Length of string to print.
                        call PR_STRING                  ; Print string (ROM).
                        ei                              ; Enable interrupts.
                        ret                             ; Return to where we hooked in.
pend



MenuFlashHook           proc
                        ex af, af'                      ; Run code that we overpatched
                        ld (hl), a                      ; to call this.
                        push de                         ; Preserve de because the routine we hooked uses it.
                        ld hl, $5855                    ; Current attribute for option 1 (keyboard).
                        ld de, $5856                    ; Location of "/REDEFINE" text.
                        ld bc, 9                        ; 9 attributes.
                        ldir                            ; Copy attributes.
                        pop de                          ; Restore de.
                        ei                              ; Enable interrupts.
                        ret                             ; Return to where we hooked in.
pend



MenuKeyHook             proc
                        jp z, OneNotPressed             ; Run code that we overpatched.
                        push af                         ; Save registers.
                        push bc
                        push de
                        push hl
                        push ix
                        call ShowRedefineKeyScreen      ; Redefine keys.
                        pop ix                          ; Restore registers.
                        pop hl
                        pop de
                        pop bc
                        pop af
                        and $F9                         ; Run code that we overpatched.
OneNotPressed:
                        bit 1, e                        ; Run code that we overpatched.
                        jp z, $7C4B                     ; Return to where we hooked in if "0" key was pressed...
                        jp $7C47                        ; ...or not pressed.
pend

