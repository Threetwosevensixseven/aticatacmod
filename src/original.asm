; original.asm

; The code in this file isn't really needed, as it just overwrites the
; original routines that are already present in game.bin.
; But it was useful to include it so I could step through those original
; routines in the Zeus emulator.

org                     MoveKeysAddr
MoveKeysOriginal        proc
                        ld a, high(K_TREWQ)             ; All five move keys are in the same row!
                        out ($FD), a
                        in a, ($FE)                     ; Read the TREWQ row.
                        ld c, a
                        rra
                        and 1
                        ld e, a
                        ld a, c
                        rla
                        and 2                           ; Return with the following values:
                        or e                            ;   - None:  $1F
                        ld e, a                         ;   - Right: $1E (bit 0 unset)
                        ld a, c                         ;   - Left:  $1D (bit 1 unset)
                        and $1C                         ;   - Down:  $1B (bit 2 unset)
                        or e                            ;   - Up:    $17 (bit 3 unset)
                        ret                             ;   - Fire:  $0F (bit 4 unset)
pend



org                     DropKeyAddr
DropKeyOriginal         proc
                        ld a, high(K_BNMSsSp & K_VCXZCs); Read both BNMSsSp and VCXZCs rows.
                        out ($FD), a
                        in a, ($FE)
                        cpl
                        and 2                           ; Check bit 1 only (Ss and Z).
                        ld ($5E20), a                   ; Save 0 (no drop) or 2 (drop).
                        ret                             ; Return.
pend



org                     PauseKeyAddr
PauseKeyOriginal        proc                            ; Always unpaused when we enter (state machine STATE 0).
                        di                              ; Disable interrupts (part of the actual pause mechanism).
                        ld a, high(K_BNMSsSp & K_VCXZCs); Read both BNMSsSp and VCXZCs rows.
                        out ($FD), a
                        in a, ($FE)
                        bit 0, a                        ; Check bit 0 only (Sp and Cs).
                        ret nz                          ; Return if neither key pressed.

                        cpl
                        and $1E                         ; Return with 0 in a
                        ret nz                          ; if neither key pressed. (Again!)
UnpauseRelease:                                         ; We are now pausing (state machine STATE 1).
                        ld a, high(K_BNMSsSp & K_VCXZCs); Read both BNMSsSp and VCXZCs rows again.
                        out ($FD), a
                        in a, ($FE)
                        bit 0, a
                        jr z, UnpauseRelease            ; Repeat until both keys are unpressed.
UnpausePress:                                           ; We are now paused (state machine STATE 2).
                        ld a, high(K_BNMSsSp & K_VCXZCs); Read both BNMSsSp and VCXZCs rows again.
                        out ($FD), a
                        in a, ($FE)
                        bit 0, a
                        jr nz, UnpausePress             ; Repeat until either key is pressed.
UnpauseRelease2:                                        ; We are now unpausing (state machine STATE 3).
                        ld a, high(K_BNMSsSp & K_VCXZCs); Read both BNMSsSp and VCXZCs rows again.
                        out ($FD), a
                        in a, ($FE)
                        bit 0, a
                        jr z, UnpauseRelease2           ; Repeat until both keys are unpressed.
                        ret                             ; We are now unpaused (state machine STATE 0).
                                                        ; Return. Interrupts will be re-enabled later(?).
pend

