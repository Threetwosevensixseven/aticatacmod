; custom.asm

; This file contains routines that add custom functionality to the game,
; and also handles the entry point from the tape loader.

org                     EntryPoint
Start                   proc
                        di                                      ; Disable interrupts whilst setting up.
                        ld bc, $7FFD                            ; 48K ROM, RAM page 0, disable bank switching.
                        ld a, 48
                        out (c), a
                        xor a                                   ; Border black.
                        out ($FE), a
                        ld sp, $5DFE                            ; Set stack.
                        ei                                      ; Game expects interrupts to be enabled.
                        jp MainMenuLoop                         ; Jump to main menu loop.
pend



ShowRedefineKeyScreen   proc
                        call Cls                                ; Clear pixels and attributes.

                        ld a, 1                                 ; 1 is the stream for the lower screen.
                        call CHAN_OPEN                          ; Open channel (ROM).
                        ld de, Copyright.Table                  ; Address of string to print.
                        ld bc, Copyright.Len                    ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).

                        ld a, 2                                 ; 2 is the stream for the upper screen.
                        call CHAN_OPEN                          ; Open channel (ROM).
                        ld de, Actions.Table                    ; Address of string to print.
                        ld bc, Actions.Len                      ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).

                        ld ix, KeyCache.Table                   ; Location to store the keycodes and masks before committing them.

                        ld a, KR0                               ; Screen row to print key.
                        ld c, 4                                 ; Width of LEFT.
                        call ReadRedefineKeys

                        ld a, KR1                               ; Screen row to print key.
                        ld c, 5                                 ; Width of RIGHT.
                        call ReadRedefineKeys

                        ld a, KR2                               ; Screen row to print key.
                        ld c, 2                                 ; Width of UP.
                        call ReadRedefineKeys

                        ld a, KR3                               ; Screen row to print key.
                        ld c, 4                                 ; Width of DOWN.
                        call ReadRedefineKeys

                        ld a, KR4                               ; Screen row to print key.
                        ld c, 10                                ; Width of USE WEAPON.
                        call ReadRedefineKeys
                        ld (MoveKeysHook.FireKeyMask), a        ; Fire Mask.
                        ld a, b
                        ld (MoveKeysHook.FireKeyCode), a        ; Fire Code.

                        ld a, KR5                               ; Screen row to print key.
                        ld c, 11                                ; Width of PICKUP/DROP.
                        call ReadRedefineKeys

                        ld a, KR6                               ; Screen row to print key.
                        ld c, 5                                 ; Width of PAUSE.
                        call ReadRedefineKeys

                        ld a, KR7                               ; Screen row to print key.
                        ld c, 4                                 ; Width of QUIT.
                        call ReadRedefineKeys

                        xor a                                   ; CLS attributes
                        ld hl, $5A60                            ; of
                        ld de, $5A61                            ; last five
                        ld bc, 32*5                             ; screen
                        ldir                                    ; rows.

                        ld de, CommitRedefine.Table             ; Address of string to print.
                        ld bc, CommitRedefine.Len               ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).
WaitForNoKeys:
                        xor a                                   ; Repeat until no keys
                        in a, ($FE)                             ; are pressed.
                        cpl
                        and %11111
                        jr nz, WaitForNoKeys
ReadYN:
                        ld bc, K_BNMSsSp                        ; Read "N" key.
                        in a, (c)
                        cpl
                        and %01000
                        jp nz, Return

                        ld bc, K_YUIOP                          ; Read "Y" key.
                        in a, (c)
                        cpl
                        and %10000
                        jr nz, Commit

                        jr ReadYN                               ; Repeat until either Y or N is pressed.
Commit:
                        ld ix, KeyCache.Table                   ; Cached keycodes and masks.

                        ld a, (ix+KeyCache.LeftCode)            ; Commit Left code.
                        ld (MoveKeysHook.LeftKeyCode), a
                        ld a, (ix+KeyCache.LeftMask)            ; Commit Left mask.
                        ld (MoveKeysHook.LeftKeyMask), a

                        ld a, (ix+KeyCache.RightCode)           ; Commit Right code.
                        ld (MoveKeysHook.RightKeyCode), a
                        ld a, (ix+KeyCache.RightMask)           ; Commit Right mask.
                        ld (MoveKeysHook.RightKeyMask), a

                        ld a, (ix+KeyCache.UpCode)              ; Commit Up code.
                        ld (MoveKeysHook.UpKeyCode), a
                        ld a, (ix+KeyCache.UpMask)              ; Commit Up mask.
                        ld (MoveKeysHook.UpKeyMask), a

                        ld a, (ix+KeyCache.DownCode)            ; Commit Down code.
                        ld (MoveKeysHook.DownKeyCode), a
                        ld a, (ix+KeyCache.DownMask)            ; Commit Down mask.
                        ld (MoveKeysHook.DownKeyMask), a

                        ld a, (ix+KeyCache.FireCode)            ; Commit Fire code.
                        ld (MoveKeysHook.FireKeyCode), a
                        ld a, (ix+KeyCache.FireMask)            ; Commit Fire mask.
                        ld (MoveKeysHook.FireKeyMask), a

                        ld a, (ix+KeyCache.DropCode)            ; Commit Drop code.
                        ld (DropKeyHook.DropKeyCode), a
                        ld a, (ix+KeyCache.DropMask)            ; Commit Drop mask.
                        ld (DropKeyHook.DropKeyMask), a

                        ld a, (ix+KeyCache.PauseCode)           ; Commit Pause code.
                        ld (PauseKeyHook.PauseKeyCode1), a
                        ld (PauseKeyHook.PauseKeyCode2), a
                        ld (PauseKeyHook.PauseKeyCode3), a
                        ld (PauseKeyHook.PauseKeyCode4), a
                        ld a, (ix+KeyCache.PauseMask)           ; Commit Pause mask.
                        ld (PauseKeyHook.PauseKeyMask1), a
                        ld (PauseKeyHook.PauseKeyMask2), a
                        ld (PauseKeyHook.PauseKeyMask3), a
                        ld (PauseKeyHook.PauseKeyMask4), a

                        ld a, (ix+KeyCache.QuitCode)            ; Commit Quit code.
                        ld (PauseKeyHook.QuitKeyCode), a
                        ld a, (ix+KeyCache.QuitMask)            ; Commit Quit mask.
                        ld (PauseKeyHook.QuitKeyMask), a

                        ld a, $C3                               ; jp nn ($C3).
                        ld (MoveKeysAddr), a                    ; Hook in the custom
                        ld hl, MoveKeysHook                     ; move key handling routine.
                        ld (MoveKeysAddr+1), hl                 ; jp MoveKeysHook.

                        ld (DropKeyAddr), a                     ; Hook in the custom
                        ld hl, DropKeyHook                      ; drop key handling routine.
                        ld (DropKeyAddr+1), hl                  ; jp MoveKeysNew.

                        ld (PauseKeyAddr), a                    ; Hook in the custom
                        ld hl, PauseKeyHook                     ; pause key handling routine
                        ld (PauseKeyAddr+1), hl                 ; jp DropKeyHook.
Return:
                        call Cls                                ; Clear screen again.
                        call MenuPrintHook                      ; Reprint the main menu screen
                        ret                                     ; (including "/REDEFINE" text").
pend



Cls                     proc
                        xor a                                   ; Empty/black/black.
                        ld hl, SCREEN                           ; SCREEN = $4000.
                        ld de, SCREEN+1
                        ld bc, SCREEN_SIZE                      ; SCREEN_SIZE = 6192 bytes.
                        ld (hl), a
                        ldir                                    ; Clear all pixels and attributes.
                        ret
pend



SavePixels              proc
                        nop
                        for Lines = 0 to 7
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ld bc, $F7
                          add hl, bc
                        next;Lines
                        nop
                        ret
pend



RestorePixels           proc
                        for Lines = 0 to 7
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ldi
                          ld bc, $F7
                          ex de, hl
                          add hl, bc
                          ex de, hl
                        next;Lines
                        ret
pend



CopyAttributes          proc
                        ld bc, 9
                        ldir
                        ret
pend



ReadRedefineKeys        proc
                        ld (RowSMC), a                          ; SMC> Save row for later (word).
                        ld (PrintKey.RowSMC), a                 ; SMC> Save row for later (letter).
                        ld (KeyFlash.RowSMC1), a                ; SMC> Save row for later (letter flash).
                        ld (KeyFlash.RowSMC2), a                ; SMC> Save row for later (action flash).
                        ld (KeyUnflash.RowSMC), a               ; SMC> Save row for later (action unflash).

                        ld a, KeyFlash.Size                     ; KeyFlash.Size is the print count minus the action flash count.
                        ld e, c
                        add a, c                                ; Add the action flash count to this for the total print count.
                        ld c, a
                        ld b, 0
                        push bc                                 ; Save flash print count for later.

                        ld a, KeyUnflash.Size                   ; KeyUnflash.Size is the print count minus the action unflash count.
                        add a, e                                ; Add the action unflash count to this for the total print count.
                        ld (UnflashSMC), a                      ; Save unflash print count for later.

                        ld a, 2                                 ; Upper screen.
                        call CHAN_OPEN                          ; Open channel (ROM).

                        ld de, KeyFlash.Table                   ; Address of string to print.
                        pop bc                                  ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).

                        ld de, KeyFlash.NoOverText              ; Address of string to print.
                        ld bc, KeyFlash.NoOverLen               ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).
WaitForNoKeys:
                        xor a                                   ; Repeat until no keys
                        in a, ($FE)                             ; are pressed.
                        cpl
                        and %11111
                        jr nz, WaitForNoKeys
ReadKeys:
                        ld bc, K_BNMSsSp                        ; Read key matrix row 0 (BNMSsSp).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_HJKLEn                         ; Read key matrix row 1 (HJKLEn).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_YUIOP                          ; Read key matrix row 2 (YUIOP).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_67890                          ; Read key matrix row 3 (67890).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key was pressed, decode it.

                        ld bc, K_54321                          ; Read key matrix row 4 (54321).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_TREWQ                          ; Read key matrix row 5 (TREWQ).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_GFDSA                          ; Read key matrix row 6 (GFDSA).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        ld bc, K_VCXZCs                         ; Read key matrix  row 7 (VCXZCs).
                        in a, (c)
                        cpl
                        and %11111
                        jr nz, Decode                           ; If any key in this row was pressed, decode it.

                        jr ReadKeys                             ; Otherwise, repeat until a key was pressed.
Decode:
                        ld c, a                                 ; b = KeyCode, c = KeyMask.
                        ld (ix+0), b                            ; Save KeyCode in KeyCache.Table.
                        ld (ix+1), c                            ; Save KeyMask in KeyCache.Table.
                        inc ix                                  ; Move KeyCache.Table to next record.
                        inc ix

                        ld a, -8                                ; Each mask bit represents
                        ld e, 8                                 ; eight KeyCode.Table records.
NextMaskBit:            add a, e
                        rrc c
                        jr nc, NextMaskBit                      ; Rotate until we find the mask bit.

                        ld c, a                                 ; Each code bit represents
                        ld e, KeyCode.Size                      ; one KeyCode.Table record.
                        ld a, -KeyCode.Size
NextCodeBit:            add a, e
                        rlc b
                        jr c, NextCodeBit                       ; Rotate until we find the code bit.

                        add a, c                                ; Lookup mask and code
                        ld c, a                                 ; in KeyCode.Table.
                        ld b, 0
                        ld hl, KeyCode.Table
                        add hl, bc                              ; Record addr = KeyCode.Table + (KeyMask * 8) + KeyCode.
                        ld a, (hl)
                        cp PrintKeyWords.Count                  ; PrintKeyWords.Count = 4.
                        jr nc, IsASCII                          ; Is A an ASCII code (larger than 3)?
IsWord:
                        add a, a                                ; *  2
                        ld e, a
                        add a, a                                ; *  4
                        add a, a                                ; *  8
                        add a, e                                ; * 10
                        ld e, a
                        ld d, b
                        ld hl, PrintKeyWords.Table+2
                        add hl, de                              ; Row addr = PrintKeyWords.Table + (KeyCode * 10) + 2.
RowSMC equ $+1:         ld (hl), SMC                            ; <SMC PrintKeyWords.Row = row number.
                        dec hl
                        dec hl
                        ld c, (hl)
                        ld b, d                                 ; bc = PrintKeyWords.Chars (7, 8 or 9).
                        inc hl
                        ex de, hl                               ; de = Address of string to print.
                        call PR_STRING                          ; Print string (ROM).
                        jr Unflash
IsASCII:
                        ld (PrintKey.CharSMC), a                ; SMC> Single letter to print.
                        ld de, PrintKey.Table                   ; Address of string to print.
                        ld bc, PrintKey.Len                     ; Length of string to print.
                        call PR_STRING                          ; Print string (ROM).
Unflash:
                        ld de, KeyUnflash.Table                 ; Address of string to print.
UnflashSMC equ $+1:     ld bc, SMC                              ; < SMC Length of string to print (MSB will always be 0).
                        call PR_STRING                          ; Print string (ROM).

                        ret
pend

