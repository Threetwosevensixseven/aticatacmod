; constants.asm

; This file contains all the constants used to address memory locations in the
; ZX Spectrum, the game code and the patch code.
; It also contains some switches to control how the assembly is done.

; Testing

EnableRedefineAtStart   equ true
EnableMove              equ EnableRedefineAtStart
EnableDrop              equ EnableRedefineAtStart
EnablePause             equ EnableRedefineAtStart
EnableRedefineScreen    equ true



; Locations

Game1Start              equ $4000                       ; Start of original game.
Game1End                equ $D509                       ; End of original game first block.
Game1Len                equ PatchEnd-Game1Start+1       ; End of original game plus patches.

PatchStart              equ Game1End+1                  ; Start of spare RAM and patches.
PatchLen                equ PatchEnd-PatchStart+1       ; End of patches.
EntryPoint              equ PatchStart                  ; Location to first jump to.

Block1Start             equ Game1Start+SCREEN_SIZE      ; First block to save to tape.
Block1Len               equ PatchEnd-Block1Start+1      ; Original game plus patches (minus screen).

Block2Start             equ $D6FE                       ; Two isolated bytes of original game.
Block2Len               equ 2

Block3Start             equ $FF10                       ; Block of original game.
Block3Len               equ $FFFF-Block3Start+1         ; at top of RAM.

MoveKeysAddr            equ $93CD                       ; Location of original MoveKeys routine.
MoveKeysCall            equ $939D                       ; Location where MoveKeys gets called.
DropKeyAddr             equ $938B                       ; Location of original DropKey routine.
PauseKeyAddr            equ $9489                       ; Location of original PauseKey routine.
MainMenuLoop            equ $7C19                       ; This can function as the entry point to the main game.
MenuPrintCall           equ $7C2C                       ; Location where menu print routine gets called.
MenuIconsAddr           equ $A311                       ; Routine that prints the main menu icons (once).
MenuTextAddr            equ $7CAF                       ; Routine that prints the main menu text (repeatedly).
MenuFlashAddr           equ $A216                       ; Routine that overprints the menu attributes (repeatedly).

AticAtacFont            equ $BF4C                       ; Location of custom font.



; Keyboard                                              ; <---------BIT----------->
                                                        ; <-4--3--2--1-------0---->
K_BNMSsSp               equ 32766                       ;   B  N  M  Symbol, Space
K_HJKLEn                equ 49150                       ;   H  J  K  L       Enter
K_YUIOP                 equ 57342                       ;   Y  U  I  O       P
K_67890                 equ 61438                       ;   6  7  8  9       0
K_54321                 equ 63486                       ;   5  4  3  2       1
K_TREWQ                 equ 64510                       ;   T  R  E  W       Q
K_GFDSA                 equ 65022                       ;   G  F  D  S       A
K_VCXZCs                equ 65278                       ;   V  C  X  Z       Caps



; Redefine

QAOP                    equ false                       ; Right-handed key set.
ZXPL                    equ true                        ; Left-handed key set.

if QAOP
  K_DownCode            equ $FD                         ; A (Down).
  K_DownMask            equ $01
  K_RightCode           equ $DF                         ; P (Right).
  K_RightMask           equ $01
  K_LeftCode            equ $DF                         ; O (Left).
  K_LeftMask            equ $02
  K_UpCode              equ $FB                         ; Q (Up).
  K_UpMask              equ $01
  K_FireCode            equ $7F                         ; M (Fire).
  K_FireMask            equ $04
  K_DropCode            equ $FE                         ; Z (Drop).
  K_DropMask            equ $02
  K_PauseCode           equ $FD                         ; F (Pause).
  K_PauseMask           equ $08
  K_QuitCode            equ $FE                         ; X (Quit).
  K_QuitMask            equ $04
endif

if ZXPL
  K_DownCode            equ $BF                         ; L (Down).
  K_DownMask            equ $02
  K_RightCode           equ $FE                         ; X (Right).
  K_RightMask           equ $04
  K_LeftCode            equ $FE                         ; Z (Left).
  K_LeftMask            equ $02
  K_UpCode              equ $DF                         ; P (Up).
  K_UpMask              equ $01
  K_FireCode            equ $7F                         ; SPACE (Fire).
  K_FireMask            equ $01
  K_DropCode            equ $BF                         ; K (Drop).
  K_DropMask            equ $04
  K_PauseCode           equ $FD                         ; F (Pause).
  K_PauseMask           equ $08
  K_QuitCode            equ $FB                         ; Q (Quit).
  K_QuitMask            equ $01
endif



; Redefine Screen

KeyStartRow             equ 2                           ; Starting at row 2,
KR0                     equ KeyStartRow+(2*0)           ; increasing by 2 rows every time.
KR1                     equ KeyStartRow+(2*1)
KR2                     equ KeyStartRow+(2*2)
KR3                     equ KeyStartRow+(2*3)
KR4                     equ KeyStartRow+(2*4)
KR5                     equ KeyStartRow+(2*5)
KR6                     equ KeyStartRow+(2*6)
KR7                     equ KeyStartRow+(2*7)
QX                      equ 8                           ; Quit message origin X
QY                      equ 8                           ; Quit message origin Y



; Tape loader
Mode3Options            equ $00006135                   ; Standard Spectrum loading colours (cyan/red, blue/yellow).



; ROM And SYSVARS
CHAN_OPEN               equ $1601                       ; Make stream A the current channel.
PR_STRING               equ $203C                       ; Print BC chars, pointed to by DE.
CHARS                   equ $5C36                       ; Sys variable pointing to font.



; Screen

SCREEN                  equ $4000                       ; Start of screen bitmap.
ATTRS_8x8               equ $5800                       ; Start of 8x8 attributes.
ATTRS_8x8_END           equ $5B00                       ; End of 8x8 attributes.
ATTRS_8x8_COUNT         equ ATTRS_8x8_END-ATTRS_8x8     ; 768.
SCREEN_SIZE             equ ATTRS_8x8_END-SCREEN        ; 6192.
BS                      equ 8
CR                      equ 13
INK                     equ 16
PAPER                   equ 17
FLASH                   equ 18
BRIGHT                  equ 19
INVERSE                 equ 20
OVER                    equ 21
AT                      equ 22
TAB                     equ 23
BLACK                   equ 0
BLUE                    equ 1
RED                     equ 2
MAGENTA                 equ 3
GREEN                   equ 4
CYAN                    equ 5
YELLOW                  equ 6
WHITE                   equ 7
SMC                     equ 0

