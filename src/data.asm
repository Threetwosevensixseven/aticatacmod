; data.asm

; This file contains all the database tables.
; Mostly text to be printed on the screen, and also a keycode lookup table
; used when printing the redefined keys.

MainMenu                proc Table:

                        db PAPER, BLACK, INK, CYAN, BRIGHT, 1, FLASH
Flashing:               db 1
                        db AT, 2, 22, "/REDEFINE"
                        db FLASH, 0

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend


ConfirmRedefine         proc Table:

                        db PAPER, BLACK, INK, WHITE, BRIGHT, 1, FLASH, 0
                        db AT, 0, 9, "ATICATAC KEYS"


                        db AT, 19, 2, INK, WHITE
                        db "REDEFINE MOD ", 37, "2017 SEVENFFF" // 37 is (c)
                        db AT, 21, 3, BRIGHT, 0
                        db "HTTP://BIT.DO/ATICATACMOD" // 37 is (c)

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



Actions                 proc Table:

                        db PAPER, BLACK, INK, CYAN, BRIGHT, 1, FLASH, 0
                        db AT, KR0, 7, "LEFT"
                        db AT, KR1, 7, "RIGHT"
                        db AT, KR2, 7, "UP"
                        db AT, KR3, 7, "DOWN"
                        db AT, KR4, 7, "USE WEAPON"
                        db AT, KR5, 7, "PICKUP/DROP"
                        db AT, KR6, 7, "PAUSE"
                        db AT, KR7, 7, "QUIT"
                        db PAPER, BLACK, INK, MAGENTA, BRIGHT, 1, FLASH, 0
                        db AT, KR0, 23, "?"
                        db AT, KR1, 23, "?"
                        db AT, KR2, 23, "?"
                        db AT, KR3, 23, "?"
                        db AT, KR4, 23, "?"
                        db AT, KR5, 23, "?"
                        db AT, KR6, 23, "?"
                        db AT, KR7, 23, "?"

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



PrintKey                proc Table:

                        db AT
RowSMC:                 db SMC
                        db 23
CharSMC                 db SMC

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



CommitRedefine          proc Table:

                        db PAPER, BLACK, INK, WHITE, BRIGHT, 1, FLASH, 0, OVER, 0
                        db AT, 19, 5, "USE THESE KEYS? "
                        db INK, WHITE, BRIGHT, 0
                        db "(Y/N)"
                        db INK, CYAN, BRIGHT, 1, FLASH, 1
                        db AT, 19, 22, "Y"
                        db AT, 19, 24, "N"

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



KeyFlash                proc Table:

NoOverText:             db OVER, 0, INK, MAGENTA, FLASH, 0
                        NoOverLen equ $-NoOverText
                        db PAPER, BLACK, BRIGHT, 1, FLASH, 1, AT
RowSMC1:                db SMC
                        db 23, "?"
                        db INK, CYAN, OVER, 1, AT
RowSMC2:                db SMC
                        db 7, "           " ; Max eleven spaces

                        Len   equ $-Table
                        Size  equ Len-11 ; Add the number of overprinting spaces at runtime
                        Count equ 1
pend



KeyUnflash              proc Table:

                        db PAPER, BLACK, INK, CYAN, BRIGHT, 1, FLASH, 0, OVER, 1, AT
RowSMC:                 db SMC
                        db 7, "           " ; Max eleven spaces

                        Len   equ $-Table
                        Size  equ Len-11 ; Add the number of overprinting spaces at runtime
                        Count equ 1
pend



Copyright               proc Table:

                        db PAPER, BLACK, INK, MAGENTA, BRIGHT, 0, FLASH, 0
                        db AT, 1, 6
                        db "WITH THANKS TO RARE"

                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



ConfirmQuit             proc Table:

                        db PAPER, BLUE, INK, WHITE, BRIGHT, 1, FLASH, 0
                        db AT, QY+0, QX+0, "         "
                        db AT, QY+1, QX+0, " ARE YOU "
                        db AT, QY+2, QX+0, "         "
                        db AT, QY+3, QX+0, "  SURE?  "
                        db AT, QY+4, QX+0, "         "
                        db AT, QY+5, QX+0, "  (Y/N)  "
                        db AT, QY+6, QX+0, "         "
                        db AT, QY+5, QX+3, INK, GREEN, FLASH, 1, "Y"
                        db AT, QY+5, QX+5, INK, RED, "N"
                        Len   equ $-Table
                        Size  equ Len
                        Count equ 1
pend



KeyCode                 proc Table:

                        ;  Key   Index     Char  Notes
                        db $00   ;   0    Space  PrintKeyWords.Table record 0
                        db $01   ;   1    Enter  PrintKeyWords.Table record 1
                        db $50   ;   2        P
                        db $30   ;   3        0
                        db $31   ;   4        1
                        db $51   ;   5        Q
                        db $41   ;   6        A
                        db $02   ;   7     Caps  PrintKeyWords.Table record 2
                        db $03   ;   8   Symbol  PrintKeyWords.Table record 3
                        db $4C   ;   9        L
                        db $4F   ;  10        O
                        db $39   ;  11        9
                        db $32   ;  12        2
                        db $57   ;  13        W
                        db $53   ;  14        S
                        db $5A   ;  15        Z
                        db $4D   ;  16        M
                        db $4B   ;  17        K
                        db $49   ;  18        I
                        db $38   ;  19        8
                        db $33   ;  20        3
                        db $45   ;  21        E
                        db $44   ;  22        D
                        db $58   ;  23        X
                        db $4E   ;  24        N
                        db $4A   ;  25        J
                        db $55   ;  26        U
                        db $37   ;  27        7
                        db $34   ;  28        4
                        db $52   ;  29        R
                        db $46   ;  30        F
                        db $43   ;  31        C
                        db $42   ;  32        B
                        db $48   ;  33        H
                        db $59   ;  34        Y
                        db $36   ;  35        6
                        db $35   ;  36        5
                        db $54   ;  37        T
                        db $47   ;  38        G
                        db $56   ;  39        V

                        struct
                          Key   ds 1
                        Size send

                        Len   equ $-Table
                        Count equ Len/Size
pend



PrintKeyWords           proc Table:

                        ; Chars  At  Col  Row   Text     ;  Index  Text
                        db    8, AT, SMC,  23, "SPACE "  ;      0  Space
                        db    8, AT, SMC,  23, "ENTER "  ;      1  Enter
                        db    7, AT, SMC,  23, "CAPS  "  ;      2  Caps
                        db    9, AT, SMC,  23, "SYMBOL"  ;      3  Symbol

                        struct
                          Chars   ds 1
                          At      ds 1
                          Col     ds 1
                          Row     ds 1
                          Text    ds 6
                        Size send

                        Len   equ $-Table
                        Count equ Len/Size
pend


KeyCache                proc Table:

                        ; KeyCode  KeyMask  Index  Action
                        db    SMC,     SMC  ;   0  Left
                        db    SMC,     SMC  ;   1  Right
                        db    SMC,     SMC  ;   2  Up
                        db    SMC,     SMC  ;   3  Down
                        db    SMC,     SMC  ;   4  Use Weapon
                        db    SMC,     SMC  ;   5  Pickup/Drop
                        db    SMC,     SMC  ;   6  Pause
                        db    SMC,     SMC  ;   7  Quit

                        struct
                          LeftCode      ds 1
                          LeftMask      ds 1
                          RightCode     ds 1
                          RightMask     ds 1
                          UpCode        ds 1
                          UpMask        ds 1
                          DownCode      ds 1
                          DownMask      ds 1
                          FireCode      ds 1
                          FireMask      ds 1
                          DropCode      ds 1
                          DropMask      ds 1
                          PauseCode     ds 1
                          PauseMask     ds 1
                          QuitCode      ds 1
                          QuitMask      ds 1
                        Size send

                        Len   equ $-Table
                        Count equ Len/Size
pend



Buffer                  proc

Pixels:                 ds 7*8*9                                ; Pixels: 7 rows of 8 lines of 9 columns
Attrs:                  ds 7*9                                  ; Attrs:  7 rows of 8 columns

pend



QRCode                   proc Table:

                        ; ColA ColB ColC ColD ColE   Line
                        db $3F, $FF, $FF, $FF, $F8  ;   0       ; QR-Code for HTTP://BIT.DO/ATICATACMOD
                        db $20, $30, $9C, $28, $08  ;   1
                        db $2F, $AB, $54, $EB, $E8  ;   2
                        db $28, $A7, $8D, $7A, $28  ;   3
                        db $28, $A7, $B9, $9A, $28  ;   4
                        db $28, $A9, $F8, $DA, $28  ;   5
                        db $2F, $A0, $77, $1B, $E8  ;   6
                        db $20, $2A, $AA, $A8, $08  ;   7
                        db $3F, $F6, $F4, $8F, $F8  ;   8
                        db $3B, $0A, $E1, $64, $18  ;   9
                        db $31, $C1, $84, $E1, $C8  ;  10
                        db $29, $1D, $62, $18, $E8  ;  11
                        db $38, $C5, $CD, $AD, $58  ;  12
                        db $27, $05, $09, $2A, $18  ;  13
                        db $39, $D9, $FB, $2B, $E8  ;  14
                        db $2F, $91, $B4, $D2, $A8  ;  15
                        db $34, $D8, $AA, $87, $D8  ;  16
                        db $25, $3B, $68, $E8, $98  ;  17
                        db $3E, $D9, $86, $DD, $B8  ;  18
                        db $3F, $24, $67, $4C, $38  ;  19
                        db $22, $F0, $61, $D9, $68  ;  20
                        db $3E, $1F, $06, $7D, $F8  ;  21
                        db $3A, $71, $16, $FD, $78  ;  22
                        db $27, $24, $FD, $51, $28  ;  23
                        db $3C, $FE, $10, $FC, $D8  ;  24
                        db $23, $0C, $09, $00, $58  ;  25
                        db $3F, $ED, $88, $6E, $E8  ;  26
                        db $20, $2A, $C0, $CA, $A8  ;  27
                        db $2F, $A9, $6B, $4E, $68  ;  28
                        db $28, $B4, $C5, $40, $88  ;  29
                        db $28, $BE, $D3, $4C, $18  ;  30
                        db $28, $A7, $D1, $8E, $48  ;  31
                        db $2F, $BD, $5E, $A3, $38  ;  32
                        db $20, $33, $91, $4E, $28  ;  33
                        db $3F, $FF, $FF, $FF, $F8  ;  34

                        struct
                          ColA  ds 1
                          ColB  ds 1
                          ColC  ds 1
                          ColD  ds 1
                          ColE  ds 1
                        Size send

                        Len   equ $-Table
                        Count equ Len/Size
pend


