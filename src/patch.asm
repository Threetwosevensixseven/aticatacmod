; patch.asm

; This file contains does the patching in of any custom routines.

                        TempOrg equ $                   ; Save org, to be restored at the end of this file,
                                                        ; since it jumps around all over the place here.
                        if EnableMove
                          org MoveKeysAddr              ; This will also get patched
                          jp MoveKeysHook               ; the first time the keys are redefined,
                        endif                           ; even if EnableMove is false.

                        if EnableDrop
                          org DropKeyAddr               ; This will also get patched
                          jp DropKeyHook                ; the first time the keys are redefined,
                        endif                           ; even if EnableDrop is false.

                        if EnablePause
                          org PauseKeyAddr              ; This will also get patched
                          jp PauseKeyHook               ; the first time the keys are redefined,
                        endif                           ; even if EnablePause is false.

                        if EnableRedefineScreen
                          org MenuTextAddr
                          jp MenuKeysHook
                          org MenuPrintCall             ; Hook the menu print routine
                          call MenuPrintHook            ; so we can add menu text afterwards.
                          noflow                        ; Tell Zeus this is a patch.
                          org $A216
                          jp MenuFlashHook
                          org $7C3F                     ; Hook the menu key reading routine, so we can
                          jp MenuKeyHook                ; show the Redefine screen when "1" is pressed.
                        endif

                        org CHARS                       ; Point the ROM print routine
                        dw AticAtacFont-256             ; at the custom Atic Atac font.

                        CalculateFontAddress(63) ; '?'  ; Macro to calculate font address
                        org FontAddress                 ; of a character to be patched in.
                        dg .XXXXX..                     ; Add a question mark to the font.
                        dg XX...XX.
                        dg XX..XXX.
                        dg ...XXX..
                        dg ........
                        dg ...XX...
                        dg ...XX...
                        dg ........

                        CalculateFontAddress(58) ; ':'  ; Macro to calculate font address
                        org FontAddress                 ; of a character to be patched in.
                        dg ........                     ; Add a colon to the font.
                        dg ........
                        dg ...XX...
                        dg ...XX...
                        dg ........
                        dg ...XX...
                        dg ...XX...
                        dg ........
                                                         ; Restore previous org, so that
                        org TempOrg                      ; it represents the end of Block1.

